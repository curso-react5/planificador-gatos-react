import {useState, useEffect} from "react"

import Mensaje from "./Mensaje"
import CerrarModal from "../img/cerrar.svg"

function Modal({setModal, animarModal, setAnimarModal, guardarGasto, gastoEditar, setGastoEditar}) {
    const [mensaje, setMensaje] = useState("")

    const [nombre, setNombre] = useState("")
    const [cantidad, setCantidad] = useState("")
    const [categoria, setCategoria] = useState("")
    const [id, setId] = useState("")
    const [fecha, setFecha] = useState("")

    useEffect(() => {
        if(Object.keys(gastoEditar).length > 0){
            setNombre(gastoEditar.nombre)
            setCantidad(gastoEditar.cantidad)
            setCategoria(gastoEditar.categoria)
            setId(gastoEditar.id)
            setFecha(gastoEditar.fecha)
        }
    }, [])

    const ocultarModal = () => {
        setAnimarModal(false)
        
        setTimeout(() => {
            setModal(false)
        }, 500);
        setGastoEditar({})
    }

    const handleSubmit = e => {
        e.preventDefault()

        if([nombre, cantidad, categoria].includes("")){
            setMensaje("Todos los campos son obligatorios")
            return;
        }
        setMensaje("")
        guardarGasto({nombre, cantidad, categoria, fecha, id})
    }
    
    return (
        <div className="modal">
            <div className="cerrar-modal">
                <img 
                    src={CerrarModal} 
                    alt="Cerrar Modal" 
                    onClick={ocultarModal}
                />
            </div>
            <form onSubmit={handleSubmit} className={`formulario ${animarModal ? "animar" : "cerrar"}`}>
                <legend>{gastoEditar.nombre ? "Editar Gasto" : "Nuevo Gasto"}</legend>

                { mensaje && <Mensaje tipo="error">{mensaje}</Mensaje> }

                <div className="campo">
                    <label htmlFor="nombre">Nombre Gasto</label>
                    <input type="text" id="nombre" placeholder="Añade el nombre del gasto: ej. Billete de Autobús" value={nombre} onChange={ e => setNombre(e.target.value) } />
                </div>

                <div className="campo">
                    <label htmlFor="cantidad">Cantidad</label>
                    <input type="number" id="cantidad" placeholder="Añade la cantidad del gasto: ej. 200" value={cantidad} onChange={ e => setCantidad(Number(e.target.value)) } />
                </div>

                <div className="campo">
                    <label htmlFor="cantidad">Categoría</label>
                    <select id="cantidad" value={categoria} onChange={ e => setCategoria(e.target.value) }>
                        <option value="">-- Seleccione --</option>
                        <option value="ahorro">Ahorro</option>
                        <option value="casa">Casa</option>
                        <option value="comida">Comida</option>
                        <option value="ocio">Ocio</option>
                        <option value="salud">Salud</option>
                        <option value="suscripciones">Suscripciones</option>
                        <option value="varios">Gastos Varios</option>
                    </select>
                </div>

                <input type="submit" value={gastoEditar.nombre ? "Guardar Cambios" : "Añadir Gasto"} />
            </form>
        </div>
    )
}

export default Modal
