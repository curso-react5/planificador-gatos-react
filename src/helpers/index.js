export const generarId = () => {
    const random = Math.random().toString(36).substr(2)
    const fecha = Date.now().toString(36)

    return random + fecha
}

export const formatearFecha = (fecha) => {
    const nuevaFecha = new Date(fecha);
    const opciones = {
        year: "numeric", 
        month: "long", 
        day: "2-digit"
    }

    return nuevaFecha.toLocaleDateString("es-ES", opciones)
}

export const formatearEuro = (cantidad) => {
    cantidad = cantidad.toLocaleString('de-DE', {
        style: 'currency', 
        currency: 'EUR', 
        minimumFractionDigits: 2 
    });

    return cantidad
}