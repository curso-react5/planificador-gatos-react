import { useState, useEffect } from 'react'

import { generarId } from './helpers'

import Header from "./components/Header"
import ListadoGastos from './components/ListadoGastos'
import Modal from './components/Modal'
import Filtros from './components/Filtros'

import IconoNuevoGasto from "./img/nuevo-gasto.svg"

function App() {
    //Gastos
    const [gastos, setGastos] = useState(
        JSON.parse(localStorage.getItem("gastos")) ?? []
    )

    //Presupuesto
    const [presupuesto, setPresupuesto] = useState(
        Number(localStorage.getItem("presupuesto")) ?? 0
    )
    const [isValidPresupuesto, setIsValidPresupuesto] = useState(
        Number(localStorage.getItem("presupuesto")) > 0 ? true : false
    )

    //Modal
    const [modal, setModal] = useState(false)
    const [animarModal, setAnimarModal] = useState(false)
    
    //Editar gasto
    const [gastoEditar, setGastoEditar] = useState({})

    //Filtrar
    const [filtro, setFiltro] = useState("")
    const [gastosFiltrados, setGastosFiltrados] = useState([])

    //LocalStorage
    useEffect(() => {
        localStorage.setItem("presupuesto", presupuesto ?? 0) 
    }, [presupuesto])

    useEffect(() => {
        localStorage.setItem("gastos", JSON.stringify(gastos) ?? [])
    }, [gastos])

    //Editar gasto
    useEffect(() => {
        if(Object.keys(gastoEditar).length > 0){
            setModal(true)

            setTimeout(() => {
                setAnimarModal(true)
            }, 500);
        }
    }, [gastoEditar])

    //Filtrar gastos
    useEffect(() => {
        if(filtro) {
            //Filtrar gastos por categoria
            setGastosFiltrados(gastos.filter( gasto => gasto.categoria === filtro))
        }
    }, [filtro])

    //Nuevo gasto
    const handleNuevoGasto = () => {
        setGastoEditar({})
        setModal(true)

        setTimeout(() => {
            setAnimarModal(true)
        }, 500);
    }

    const guardarGasto = gasto => {
        //Actualizar existente
        if(gasto.id){
            const gastosActualizados = gastos.map(gastoState => gastoState.id === gasto.id ? gasto : gastoState)
            setGastos(gastosActualizados)  
            setGastoEditar({})
        } 
        //Crear nuevo
        else {
            gasto.id = generarId()
            gasto.fecha = Date.now();
            setGastos([...gastos, gasto])
        }

        //Cerrar modal
        setAnimarModal(false)
        setTimeout(() => {
            setModal(false)
        }, 500);
    }

    //Eliminar gasto
    const eliminarGasto = id => {
        const gastosActualizados = gastos.filter(gasto => gasto.id !== id)
        setGastos(gastosActualizados)
    }

    return (
        <div className={modal ? "fijar" : undefined}>
            <Header
                gastos={gastos}
                setGastos={setGastos}
                presupuesto={presupuesto}
                setPresupuesto={setPresupuesto}
                isValidPresupuesto={isValidPresupuesto}
                setIsValidPresupuesto={setIsValidPresupuesto}
            />

            {isValidPresupuesto && (
                <>           
                    <main>
                        <Filtros 
                            filtro={filtro}
                            setFiltro={setFiltro}
                        />
                        <ListadoGastos 
                            gastos={gastos}
                            setGastoEditar={setGastoEditar}
                            eliminarGasto={eliminarGasto}
                            filtro={filtro}
                            gastosFiltrados={gastosFiltrados}
                        />
                    </main>
                    <div className="nuevo-gasto">
                        <img
                            src={IconoNuevoGasto}
                            alt="Icono Nuevo Gasto"
                            onClick={handleNuevoGasto}
                        />
                    </div>
                </>
            )}

            {modal &&
                <Modal
                    setModal={setModal}
                    animarModal={animarModal}
                    setAnimarModal={setAnimarModal}
                    guardarGasto={guardarGasto}
                    gastoEditar={gastoEditar}
                    setGastoEditar={setGastoEditar}
                />
            }
        </div>
    )
}

export default App
